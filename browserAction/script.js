(function() {
    "use strict";

    const s = new sigma('container');

    const storage = browser.storage.local;

    let data = [];
    storage
        .get('visited')
        .then( read => {
            data = read['visited'] || [];
            refreshData(data);
        });

    let areSameEdge = function (pair1 = {}, pair2 = {}) {
        return pair1['source'] === pair2['source'] && pair1['target'] === pair2['target'];
    };

    let edgeAlreadyExist = function (pair, visitedList = []) {
        return visitedList.some(visitedPair => areSameEdge(pair, visitedPair));
    };

    let extractNodes = function (data = []) {
        let nodes = [];
        data.forEach(e => {
            if (!nodes.includes(e.location)) {
                nodes.push(e.location);
            }
            if (e.referrer && !nodes.includes(e.referrer)) {
                nodes.push(e.referrer);
            }
        });
        return nodes;
    };

    let extractEdges = function (data = []) {
        let edges = [];
        data.forEach(e => {
            const edgeToAdd = { source: e.location, target: e.referrer }
            if (e.location && e.referrer && !edgeAlreadyExist(edgeToAdd, edges)) {
                edges.push(edgeToAdd);
            }
        });
        return edges;
    };

    let refreshData = function (data = []) {

        extractNodes(data).forEach(node => {
            s.graph.addNode({ id: node, label: node, size: 2, x: 0, y: 0 });
        });

        extractEdges(data).forEach(edge => {
            s.graph.addEdge({ id: `e${edge.source}${edge.target}`, source: edge.source, target: edge.target})
        });

        s.refresh();
        s.startNoverlap({
            nodes: s.graph.nodes()
        })
    };
})();
