// Put all the javascript code here, that you want to execute in background.
(function() {
    "use strict";

    browser.browserAction.onClicked.addListener(() => {
        const createData = {
            type: "detached_panel",
            url: "browserAction/index.html",
            width: 1024,
            height: 800
        };
        const creating = browser.windows.create(createData);
    });
})();