# WIKI-GRAPH

wiki-graph is a browser extension that aims to provide a new point of view on your browsing session on wikipedia website.

## What it does

Browse wikipedia website like you use to do, go from page to page, click on every link you're interested in 
and open wiki-graph panel to display the graph of your browsing session.

## Features 

`WIP`