(function() {
    "use strict";

    const localStorage = browser.storage.local;

    const currentPageUrl = document.documentURI;
    console.log(`visiting url : ${currentPageUrl}`);

    let areSamePair = function (pair1 = {}, pair2 = {}) {
        return pair1['location'] === pair2['location'] && pair1['referrer'] === pair2['referred'];
    };

    let pairAlreadyExist = function (pair, visitedList = []) {
        return visitedList.some(visitedPair => areSamePair(pair, visitedPair))
    };

    let onceValueIsRetrievedFromStorage = function (storedData) {
        let visitedList = storedData['visited'] || [];

        let currentVisited = { 'location': currentPageUrl, 'referrer': document.referrer };

        // Add current visited pair if it does not already exist
        if (pairAlreadyExist(currentVisited, visitedList)) {
            return;
        }

        visitedList.push(currentVisited);
        localStorage
            .set({ 'visited': visitedList })
            .then(
                () => { console.log('inserted data to local storage'); },
                error => { console.error(`insertion error ${error}`); });
    };

    // Retrieve visited pages
    localStorage
        .get('visited')
        .then(onceValueIsRetrievedFromStorage);
})();
